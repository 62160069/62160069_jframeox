
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BmWz
 */
public class TestWriteObject {

    public static void main(String[] args) {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        Player x = new Player('x');
        Player o = new Player('o');

        x.win();
        o.lose();
        x.draw();
        o.draw();

        System.out.println(x);
        System.err.println(o);
        try {
            file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(x);
            oos.writeObject(o);

            fos.close();
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
                oos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
