
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BmWz
 */
public class Table implements Serializable {

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastRow;
    private int lastCol;
    private Player winner;
    private boolean finish = false;
    int turn = 0;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
        currentPlayer = X;
    }

    public void showTable() {
        System.out.println(" 1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println();
        }
    }

    public char getRowCol(int row, int col) {
        return table[row][col];
    }

    public boolean setRowCol(int row, int col) {
        if (isFinish()) {
            return false;
        }
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            turn++;
            checkWin();
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkCross() {
        if ((table[0][0] == currentPlayer.getName() && table[1][1] == currentPlayer.getName() && table[2][2] == currentPlayer.getName())
                || (table[0][2] == currentPlayer.getName() && table[1][1] == currentPlayer.getName() && table[2][0] == currentPlayer.getName())) {
            finish = true;
            winner = currentPlayer;
            setStatWinLose();
        }
    }

    public void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkDrew() {
        if (turn == 9) {
            winner = null;
            finish = true;
            playerO.draw();
            playerX.draw();
        }
    }

    void checkWin() {
        checkCol();
        checkRow();
        checkCross();
        checkDrew();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }
}
