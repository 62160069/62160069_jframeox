
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BmWz
 */
public class Player implements Serializable {

    private char name;
    private int win;
    private int lose;
    private int drew;

    public Player(char name) {
        this.name = name;

    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDrew() {
        return drew;
    }

    public void win() {
        win++;
    }

    public void draw() {
        drew++;
    }

    public void lose() {
        lose++;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", drew=" + drew + '}';
    }

}
